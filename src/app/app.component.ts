import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IEmployee } from 'ng2-org-chart';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { JSDocCommentStmt } from '@angular/compiler';
import { RegisterFormService } from './register-form.service';
declare var $: any;


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.less']
})
export class AppComponent implements OnInit {

  registerForm: FormGroup;
  department: string[] = ['Opertations Management', 'Finance', 'IT', 'Maintenance', 'HR'];
  submitted = false;

  constructor(private fb: FormBuilder, private registerUser: RegisterFormService) {}

  ngOnInit() {
    this.registerForm = this.fb.group({
      department: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required]
    });
  }



  // getting access to fields
  get f() { return this.registerForm.controls; }


  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    alert('Name: ' + this.registerForm.value.firstName + ' ' + this.registerForm.value.lastName +
          '\nDepartment: ' + this.registerForm.value.department);

    console.log(JSON.stringify(this.registerForm.value));

    this.registerUser.addUser().subscribe(
      data => {
        console.log('Sent!', data);
      },
      error => {
        console.log('Error!', error);
      }
    );

  }
}
